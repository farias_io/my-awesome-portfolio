import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Portfolio from './pages/Portfolio';
import Main from './pages/Main';
import Cv from './pages/Cv';

const AppRouter = () => {
  return (
    <Routes>
      <Route exact path="/" element={<Main />} />
      <Route exact path="/portfolio" element={<Portfolio />} />
      <Route exact path="/cv" element={<Cv />} />
    </Routes>
  );
};

export default AppRouter;
