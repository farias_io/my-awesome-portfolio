import {
  Box,
  Button,
  Center,
  ChakraProvider,
  Container,
  Divider,
  Heading,
  Img,
  Link,
  SimpleGrid,
  Text,
  Tooltip,
} from '@chakra-ui/react';
import Navbar from './Navbar';
import Footer from './Footer';

export default function Cv() {
  return (
    <ChakraProvider>
      <Container padding="4" maxW="container.lg">
        <Navbar />
        <Box padding={4}>
          <Heading as="h3" size="lg">
            cv
          </Heading>
          <Divider />
          <Text>
            Si quieres el cv o simplemente contactarte conmigo, puedes escribir
            un correo a
          </Text>
          <SimpleGrid columns={2} padding={4}>
            <Box>
              <Center h={'50vh'}>
                <Link href="mailto:farias@roboc.lol&subject=CV">
                  <Tooltip label="Haz clic sobre el botón para enviar un correo">
                    <Button padding={30} variant="solid" colorScheme="gray">
                      <Heading as="h2" size="xl" fontFamily={'Monospace'}>
                        farias@roboc.lol
                      </Heading>
                    </Button>
                  </Tooltip>
                  <Text textAlign={'center'}>
                    (solo haz clic sobre el botón para enviar un correo)
                  </Text>
                </Link>
              </Center>
            </Box>
            <Box>
              <Center>
                <Img src="/assets/img/undraw_letter_re_8m03.svg" height={400} />
              </Center>
            </Box>
          </SimpleGrid>
        </Box>
        <Footer />
      </Container>
    </ChakraProvider>
  );
}
