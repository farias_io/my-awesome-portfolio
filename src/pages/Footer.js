import {
  Box,
  Center,
  Divider,
  Flex,
  Image,
  Link,
} from '@chakra-ui/react';

export default function Footer() {
  return (
    <>
      <Divider />
      <Box>
        <Center>
          <Flex>
            <Link href="https://github.com/fariascl" target='_blank'>
              <Image
                src="/assets/img/github-square-svgrepo-com.svg"
                height={9}
                paddingTop={2}
              />
            </Link>
            <Link href="https://linkedin.com/in/fariascl" target='_blank'>
              <Image
                src="/assets/img/linkedin-svgrepo-com.svg"
                height={10}
                paddingTop={1}
              />
            </Link>
          </Flex>
        </Center>
      </Box>
    </>
  );
}
