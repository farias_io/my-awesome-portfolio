import * as React from 'react';
import {
  Box,
  ChakraProvider,
  Code,
  Container,
  Divider,
  Heading,
  Image,
  Img,
  Link,
  SimpleGrid,
  Text,
  Tooltip,
} from '@chakra-ui/react';
import Navbar from './Navbar';
import Footer from './Footer';

export default function Main() {
  const techs = [
    {
      name: 'PHP',
      image: '/assets/img/php-svgrepo-com.svg',
    },
    {
      name: 'Python',
      image: '/assets/img/python-127-svgrepo-com.svg',
    },
    {
      name: 'Java',
      image: '/assets/img/java-svgrepo-com.svg',
    },
    {
      name: 'JavaScript',
      image: '/assets/img/javascript-155-svgrepo-com.svg',
    },
    {
      name: 'Nim',
      image: '/assets/img/nim-svgrepo-com.svg',
    },
    {
      name: 'Go',
      image: '/assets/img/golang-svgrepo-com.svg',
    },
    {
      name: 'Shell Scripting (Bash, Sh)',
      image: '/assets/img/bash-icon-svgrepo-com.svg',
    },
    {
      name: 'Laravel',
      image: '/assets/img/laravel-svgrepo-com.svg',
    },
    {
      name: 'Flask',
      image: '/assets/img/flask-svgrepo-com.svg',
    },
    {
      name: 'ExpressJS',
      image: '/assets/img/express-svgrepo-com.svg',
    },
    {
      name: 'React (JS y Native)',
      image: '/assets/img/reactjs-fill-svgrepo-com.svg',
    },
    {
      name: 'MariaDB / MySQL',
      image: '/assets/img/mariadb-svgrepo-com.svg',
    },
    {
      name: 'PostgreSQL',
      image: '/assets/img/postgresql-svgrepo-com.svg',
    },
    {
      name: 'MongoDB',
      image: '/assets/img/mongodb-svgrepo-com.svg',
    },
    {
      name: 'Docker',
      image: '/assets/img/docker-svgrepo-com.svg',
    },
  ];

  return (
    <ChakraProvider>
      <Container padding="4" maxW="container.lg">
        <Navbar />
        <Box padding={4}>
          <Heading as="h3" size="lg">
            Hola
          </Heading>
          <Divider />
          <SimpleGrid padding={4} spacing={'10px'} columns={2}>
            <Box>
              <Text fontSize="lg" align={'justify'}>
                Mi nombre es{' '}
                <Tooltip label="Ale, Jano, Janito...">Alejandro</Tooltip>. Soy
                programador y amante de GNU/Linux. Escribo en mi blog llamado <Link href="https://8loop.roboc.lol/blog"><Text as={'span'} fontWeight={'bold'}>Bucle Infinito</Text></Link>.
                Programo cosas como hobby, estudios y trabajo. Tengo un par de
                proyectos en Internet que constantemente estoy desarrollando
                (<Link href="https://tmp.roboc.lol" target="_blank"><Text as={'span'} fontWeight={'bold'}>/tmp</Text></Link>, <Link href="http://patentevalida.roboc.lol/" target="_blank"><Text as={'span'} fontWeight={'bold'}>patentevalida</Text></Link>). También me interesa la
                Seguridad Informática aunque esto último lo he aprendido de
                forma autodidacta :). En mis tiempos libres programo y colaboro
                en distintos proyectos.
              </Text>
            </Box>
            <Box>
              <Tooltip label="Un perrito, porque me gustan los perros 🐶">
                <Image src="/assets/img/undraw_dog_c7i6.svg" height={300} />
              </Tooltip>
            </Box>
          </SimpleGrid>
        </Box>

        <Box padding={4}>
          <Heading as="h3" size="lg">
            Tecnologías
          </Heading>
          <Divider />
          <SimpleGrid padding={4} spacing={'10px'} columns={7}>
            {techs.map((tech, index) => (
              <Box key={index}>
                {tech.image && (
                  <Tooltip label={tech.name}>
                    <Img src={tech.image} height={'50px'} />
                  </Tooltip>
                )}
              </Box>
            ))}
          </SimpleGrid>
        </Box>
        <Box padding={4}>
          <Heading as="h3" size="lg">
            Contacto rápido
          </Heading>
          <Divider />
          Correo: <Code>farias@roboc.lol</Code>
        </Box>
        <Footer />
      </Container>
    </ChakraProvider>
  );
}
