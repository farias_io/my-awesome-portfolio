import {
  Box,
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Center,
} from '@chakra-ui/react';

export default function Navbar() {
  return (
    <Box>
      <Center>
        <Breadcrumb>
          <BreadcrumbItem>
            <BreadcrumbLink href="/">Inicio</BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink href="/portfolio">Portafolio</BreadcrumbLink>
          </BreadcrumbItem>

          <BreadcrumbItem>
            <BreadcrumbLink href="/cv">CV</BreadcrumbLink>
          </BreadcrumbItem>
        </Breadcrumb>
      </Center>
    </Box>
  );
}
