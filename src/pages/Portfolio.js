import * as React from 'react';
import {
  Box,
  Button,
  Card,
  CardBody,
  CardFooter,
  ChakraProvider,
  Container,
  Divider,
  Heading,
  Image,
  Img,
  Link,
  Stack,
  Table,
  TableContainer,
  Tag,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tr,
} from '@chakra-ui/react';
import Navbar from './Navbar';
import Footer from './Footer';

export default function Portfolio() {
  const projects = [
    {
      title: 'ReportAdmin',
      description:
        'Dashboard de administración ReportApp, proyecto de título 2023',
      //image: '/assets/img/projects/reportadmin.png',
      image: '/assets/img/projects/reportadmin.png',
      techs: ['React JS', 'Laravel'],
      link: '',
    },
    {
      title: 'ReportApp',
      description: 'App móvil para enviar reports, proyecto título 2023',
      //image: '/assets/img/projects/reportapp.png',
      image: '/assets/img/projects/reportapp.jpg',
      techs: ['React Native', 'Laravel'],
      link: '',
    },
    {
      title: '/tmp',
      description: 'Sistema simple de almacenamiento de archivos en PHP',
      //image: '/assets/img/projects/tmp',
      image: '/assets/img/projects/tmp.png',
      techs: ['PHP'],
      link: 'https://github.com/fariascl/tmp',
      demo: 'https://tmp.roboc.lol',
    },
  ];

  const projects_git = [
    {
      name: 'pyemailscraper',
      description:
        'Librería en Python para scrapear direcciones de correo electrónico en archivos y sitios web',
      tech: 'Python',
      link: 'https://github.com/fariascl/pyemailscraper',
      demo: '',
    },
    {
      name: 'patenteschile',
      description:
        'Librería en Python para generar patentes vehiculares chilenas',
      tech: 'Python',
      link: 'https://github.com/fariascl/patenteschile',
      demo: '',
    },
    {
      name: 'isbnpy',
      description:
        'Librería para obtener datos relacionados al número estándar internacional de libro (ISBN) ',
      tech: 'Python',
      link: 'https://github.com/fariascl/isbnpy',
      demo: '',
    },
    {
      name: 'roboc',
      description: 'Bot experimental para Discord',
      tech: 'Python',
      link: 'https://github.com/fariascl/roboc',
      demo: '',
    },
  ];

  return (
    <ChakraProvider>
      <Container padding="4" maxW="container.lg">
        <Navbar />
        <Box padding={4}>
          <Heading as="h3" size="lg">
            Portafolio
          </Heading>
          <Divider />
          <Text padding={3}>Algunos de los proyectos que he desarrollado son... </Text>
          <TableContainer padding={2}>
            <Table size="sm">
              <Thead>
                <Tr>
                  <Th>Título</Th>
                  <Th>Estado</Th>
                  <Th isNumeric>Repo</Th>
                </Tr>
              </Thead>
              <Tbody>
                {projects_git.map((project, index) => (
                  <Tr key={index}>
                    <Td>{project.name}</Td>
                    <Td>{project.description}</Td>
                    <Td>
                      <Link href={project.link}>
                        <Img
                          src="/assets/img/github-142-svgrepo-com.svg"
                          height={5}
                        />
                      </Link>
                    </Td>
                  </Tr>
                ))}
              </Tbody>
              <Tfoot></Tfoot>
            </Table>
          </TableContainer>

          {projects.map((project, index) => (
            <Card
              key={index}
              direction={{ base: 'column', sm: 'row' }}
              overflow="hidden"
              variant="outline"
              margin={2}
            >
              {project.image && (
                <Image
                  objectFit="cover"
                  maxW={{ base: '100%', sm: '200px' }}
                  src={project.image}
                  alt="Caffe Latte"
                />
              )}
              <Stack>
                <CardBody>
                  <Heading size="md" paddingBottom={2}>
                    {project.title}
                  </Heading>
                  {project.techs.map(tech => (
                    <Tag marginEnd={2}>{tech}</Tag>
                  ))}

                  <Text py="2">{project.description}</Text>
                </CardBody>
                <CardFooter>
                  <Button variant="solid" colorScheme="white">
                    <Img
                      src="/assets/img/github-142-svgrepo-com.svg"
                      height={10}
                    />
                  </Button>
                  {project.demo ? (
                    <Button variant="solid" colorScheme="white">
                      <Link href={project.demo}>
                        <Img src="/assets/img/link-svgrepo-com.svg" height={10} />
                      </Link>
                    </Button>
                  ) : (
                    ' '
                  )}
                </CardFooter>
              </Stack>
            </Card>
          ))}
        </Box>
        <Divider />
        <Footer />
      </Container>
    </ChakraProvider>
  );
}
